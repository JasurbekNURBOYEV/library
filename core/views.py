from django.shortcuts import render
from core.models import Book


def home(request, *args, **kwargs):
    template = 'home.html'
    return render(request, template_name=template)


def books(request, *args, **kwargs):
    template = 'books.html'
    book_groups = []
    all_books = list(Book.all())

    for i in range(0, len(all_books), 2):
        _book_group = [all_books[i]]
        if len(all_books) > i+1:
            _book_group.append(all_books[i+1])
        book_group = []
        for book in _book_group:
            book_group.append(
                {
                    'cover': book.cover.url, 'name': book.name,
                    "description": book.description or '', 'url': book.file.url,
                    "categories": ", ".join([i.name for i in book.categories.all()])
                }
            )
        book_groups.append(book_group)
    return render(request, template_name=template, context={'book_groups': book_groups})
