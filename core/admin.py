from django.contrib import admin
from django.contrib.admin import display
from django.utils.safestring import mark_safe

from core.models import *


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_time'

    @display(description='kategoriya')
    def get_categories(self, obj):
        return mark_safe("<br>".join([x.name for x in obj.categories.all()]) or 'umumiy')

    @display(description='muqova')
    def muqova(self, obj):
        return mark_safe('<img src="%s" title="%s" style="height:160px; padding-left:10px;"/><p>%s</p>' % (obj.cover.url, obj.name, obj.name))

    @display(description='nomi')
    def nomi(self, obj):
        return obj.name

    list_display = [
        'muqova',
        'get_categories',
        'id',
    ]
    search_fields = [
        'name',
    ]

    exclude = [
        'created_time',
        'last_updated_time',
    ]


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_time'

    @display(description='nomi')
    def nomi(self, obj):
        return obj.name

    list_display = [
        'nomi',
        'id',
    ]
    search_fields = [
        'name',
    ]

    exclude = [
        'created_time',
        'last_updated_time',
    ]
