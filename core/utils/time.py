from django.utils import timezone


def get_now(date: bool = False):
    now = timezone.now().replace(tzinfo=timezone.get_current_timezone())
    return now.date() if date else now
