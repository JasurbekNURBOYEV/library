from django.urls import path

from core.views import home, books

urlpatterns = [
    path('', home),
    path('books/', books),
]
